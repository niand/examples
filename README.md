# README #

This README describes how to get the application up and running.

### What is this repository for? ###

Example how to do:

pub/sub notification with ActiveMQ virtual topics

logging, use of correlation/message/causation id's

use of Camel routes 

### How do I get set up? ###

Prerequisites:

Postgres 9.x db server with "im" and sport databases,(empty) schemas and users created. (See application.properties for details)

ActiveMQ 12.x up an running on localhost:61616

Java 1.8

STS (recommended)

### Steps ###

Start "SPORT"

cd examples/interfaces/ifc-sport;
mvn spring-boot:run

Start "IM" (new window)

cd examples/interfaces/ifc-im;
mvn spring-boot:run

"Sport" web app: http://localhost:8082/sport/

"IM" web app: http://localhost:8083/im/