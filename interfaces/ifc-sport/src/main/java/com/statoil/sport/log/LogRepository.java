package com.statoil.sport.log;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

public interface LogRepository extends Repository<LogEntry, Long> {
	
	Page<LogEntry> findAll(Pageable pageable);
	LogEntry findOne(Long id);
	LogEntry save(LogEntry entry);
	List<LogEntry> findByMessageMessageId(String messageId);
	
}
