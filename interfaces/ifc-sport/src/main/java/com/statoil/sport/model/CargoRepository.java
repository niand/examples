package com.statoil.sport.model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

public interface CargoRepository extends Repository<Cargo, Long> {
	Cargo findOne(Long id);
	Cargo findByCargoNoAndDelNo(String cargoNo, String delNo);
	Cargo save(Cargo cargo);	
	Page<Cargo> findAll(Pageable pageable);
}
