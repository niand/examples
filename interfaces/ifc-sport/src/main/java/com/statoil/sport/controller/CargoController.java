package com.statoil.sport.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.statoil.sport.model.Cargo;
import com.statoil.sport.service.CargoService;

/**
 * This is a controller serving HTML (based on Thymeleaf template "templates/cargo.html"), 
 * and is thus not a part of the SPORT API. 
 *
 */
@Controller
public class CargoController {

	@Autowired
	private CargoService cargoService;

	
	@GetMapping("/")
	public String cargo(Model model) {
		model.addAttribute("cargo", new Cargo());
		return "cargo";
	}
	
	@PostMapping("/cargo")
	public String cargoSubmit(@ModelAttribute Cargo cargo) {	
		if (cargo.getId() == 0) {
			cargo = cargoService.createCargo(cargo);
		} else {
			cargo = cargoService.updateCargo(cargo);
		}
		return "redirect:/";
	}

}
