package com.statoil.sport.ifc;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Represents a movement")
public class Movement {
	
	public class Quantity {		
		@ApiModelProperty(value = "The total physical quantity", example = "12758.13", required=true)
		public Double qty;
		@ApiModelProperty(value = "The quantity unit", example = "M3", required=true)
		public String unit;	
		@ApiModelProperty(value = "Type (source) of quantity", allowableValues="Planned, Measured, Calculated", example = "Measured")
		public String type;
	}
	
	public class PhysicalMovement {		
		@ApiModelProperty(value = "The quality", example = "BU0KA1", required=true)
		public String quality;
		@ApiModelProperty(value = "The quantity/quantities in different units", required=true)
		public Quantity[] quantities;
	}
	
	public class FieldSplit {
		@ApiModelProperty(value = "The field", required=true)
		String field;
		@ApiModelProperty(value = "The quantity/quantities of this origin and field in different units", required=true)
		public Quantity[] quantities;
		@ApiModelProperty(value = "The percentage of the total B/L volume", required=true)
		public Double blPercentage;			
	}
	
	public class Origin {			
		@ApiModelProperty(value = "Origin", example = "State", allowableValues="State, Statoil, Purchase", required=true)
		public String origin;
		@ApiModelProperty(value = "The (aggregated) quantity/quantities in different units", required=true)
		public Quantity[] quantities;
		@ApiModelProperty(value = "The percentage of the total B/L volume", required=true)
		public Double blPercentage;
		@ApiModelProperty(value = "The percentage/volume split per field", required=false)
		public FieldSplit[] fieldSplit;
	}
	
	public class TitleTracking {
			public TitleTracking() {}
		
			@ApiModelProperty(value = "The seller (as defined in the source system)", example = "Statoil")
			public String seller;
			@ApiModelProperty(value = "The buyer (as defined in the source system)", example = "REFMONG")
			public String buyer;
			@ApiModelProperty(value = "The title transfer date", example = "2017-08-10T12:00")
			public String titleTransferDate;
			@ApiModelProperty(value = "Sequence number, mechanism for tracking a deal chain", example = "1")
			public int sequence;
	}			
	
	@ApiModelProperty(value = "Message type", required = true, allowableValues="LoadFromStorage, DischargeToStorage, InTankTransfer")
	public String message;
	@ApiModelProperty(value = "Message version", example = "1.1", required = true)
	public String messageVersion;
	@ApiModelProperty(value = "The external (source system) cargo identifier", required = true, example="251100")
	public String referenceKey;	
	@ApiModelProperty(value = "The external (source system) movement identifier", required = true, example="251100-1")
	public String movementReferenceKey;	
	@ApiModelProperty(value = "The source system", required = true, example = "SPORT")
	public String sourceSystem;	
	@ApiModelProperty(value = "Movement state", required = true, allowableValues = "Planned, Actual", example="Planned")
	public String state;
	@ApiModelProperty(value = "The storage name (as defined in the source system)", example = "REYKJ ODR")
	public String storage;
	@ApiModelProperty(value = "Transport mode", allowableValues="Barge, Vessel, Pipeline, Truck, None")	
	public String transportMode;
	@ApiModelProperty(value = "Vessel name", example = "CITRUS EXPRESS")	
	public String vessel;
	@ApiModelProperty(value = "COD (Completion of Discharge) date")
	public String codDate;
	@ApiModelProperty(value = "NOR (Notice of readiness) date")
	public String norDate;
	@ApiModelProperty(value = "BL (Bill of Lading) date")
	public String blDate;
	@ApiModelProperty(value = "Description of the total physical movement")
	public PhysicalMovement physicalMovement;
	@ApiModelProperty(value = "The origin of this movement")
	public Origin[] origin;	
	@ApiModelProperty(value = "Titletracking")
	public TitleTracking[] titleTracking;
	
}
