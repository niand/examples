package com.statoil.sport.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.statoil.sport.ifc.Movement;
import com.statoil.sport.log.LogEntry;
import com.statoil.sport.log.LogRepository;
import com.statoil.sport.message.Message;
import com.statoil.sport.service.CargoService;

/**
 * REST controller serving the SPORT Movement API. 
 *
 */
@RestController
public class MovementController {

	@Autowired
	private CargoService cargoService;
	
	@Autowired
	private LogRepository logRepo;
	
	@GetMapping("/api/movements")
	public List<Movement> movements(
			@RequestParam(value="page", defaultValue = "0") String page,
			@RequestParam(value="pageSize", defaultValue = "10") String pageSize) {
		
		return cargoService.getMovements(page, pageSize).getContent();			
	}
	
	@GetMapping("/api/movement")
	public ResponseEntity<Movement> movement(
			@RequestParam(value="cargoNo") String cargoNo,
			@RequestParam(value="delNo") String delNo,
			@RequestHeader(value = "CorrelationID", defaultValue = "") String correlationId,
			@RequestHeader(value = "CausationID", defaultValue = "") String causationId,
			@RequestHeader(value = "MessageID", defaultValue = "") String messageId) {
					
		Message request = Message
				.recreate(messageId, correlationId, null)
				.payload()
					.add("cargoNo", cargoNo)
					.add("delNo", delNo)
					.done();
							
		LogEntry entry = new LogEntry(request, "Received movement Request");					
		logRepo.save(entry);
		
		Movement movement = cargoService.getMovement(cargoNo, delNo);
		
		Message responseMessage = Message
				.createFrom(request)
				.payload(movement);
		
		LogEntry responseEntry = new LogEntry(responseMessage, "Sent response for cargoNo/delNo: " + cargoNo + "/" + delNo);		
		
		ResponseEntity<Movement> response = ResponseEntity.ok()				
				.header("MessageID", responseMessage.messageId)
				.header("CorrelationID", responseMessage.correlationId)
				.header("CausationID", responseMessage.causationId)
				.body(movement);				
							
		logRepo.save(responseEntry);
		
		return response;			
	}


		
}
