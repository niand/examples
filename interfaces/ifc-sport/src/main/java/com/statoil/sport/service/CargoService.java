package com.statoil.sport.service;


import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.statoil.sport.ifc.Movement;
import com.statoil.sport.ifc.MovementNotification;
import com.statoil.sport.log.LogEntry;
import com.statoil.sport.log.LogRepository;
import com.statoil.sport.message.Message;
import com.statoil.sport.model.Cargo;
import com.statoil.sport.model.CargoRepository;

@Service
public class CargoService {
	
	@Produce(uri = "direct:MovementNotificationStart")
	private ProducerTemplate movementNotificationProducer;
	
	@Produce(uri = "direct:OpDetailsNotificationStart")
	private ProducerTemplate opDetailsNotificationProducer;
	
	@Autowired
	private LogRepository logRepo;
	
	@Autowired
	private CargoRepository cargoRepo;
	
	@Transactional
	public Cargo createCargo(Cargo cargo) {
		cargo.setId(System.currentTimeMillis());
		return saveCargo(cargo, "Cargo created, publishing notifications");		
	}
	
	@Transactional
	public Cargo updateCargo(Cargo cargo) {
		return saveCargo(cargo, "Cargo updated, publishing notifications");
	}
	
	public Page<Movement> getMovements(String page, String pageSize) {
		
		Page<Cargo> cargoPage = cargoRepo.findAll(
					new PageRequest(
							Integer.parseInt(page), 
							Integer.parseInt(pageSize)));
		
		Page<Movement> movementPage = cargoPage.map(new Converter<Cargo, Movement>() {
			@Override
			public Movement convert(Cargo cargo) {
				return mapCargoToMovement(cargo);
			}			
		});
		
		return movementPage;
	}
	
	@Transactional(readOnly=true)
	public Movement getMovement(String cargoNo, String delNo) {
		Cargo cargo = cargoRepo.findByCargoNoAndDelNo(cargoNo, delNo);
		Movement movement = mapCargoToMovement(cargo);		
		return movement;	
	}
	
	private Cargo saveCargo(Cargo cargo, String logMessage) {
		
		MovementNotification notification = new MovementNotification();
		notification.cargoNo = cargo.getCargoNo();
		notification.deliveryNo = cargo.getDelNo();
		notification.deliveryTerms = cargo.getDeliveryTerms();
		
		Message notificationMessage = Message.createNew().payload(notification);
						
		Cargo persistedCargo = cargoRepo.save(cargo);		
		saveCargoLogEntry(notificationMessage, logMessage);
		movementNotificationProducer.asyncSendBody(movementNotificationProducer.getDefaultEndpoint(), notificationMessage);
		opDetailsNotificationProducer.asyncSendBody(opDetailsNotificationProducer.getDefaultEndpoint(), notificationMessage);
		return persistedCargo;
	}
	
	private LogEntry saveCargoLogEntry(Message message, String description) {				
		LogEntry logEntry = new LogEntry(message, description);		
		logRepo.save(logEntry);	
		return logEntry;
	}
	
	/*
	 * Add some dummy data
	 */
	private Movement mapCargoToMovement(Cargo cargo) {
		Movement movement = new Movement();
		movement.sourceSystem = "SPORT";
		movement.messageVersion = "1.1";
		movement.message = "LoadFromStorage";
		movement.blDate = "18102015T12:12";
		movement.movementReferenceKey = cargo.getCargoNo() + "/" + cargo.getDelNo();
		movement.referenceKey = cargo.getCargoNo();
		movement.state = "Planned";
		movement.transportMode = "Vessel";
		movement.titleTracking = new Movement.TitleTracking[1];
		movement.titleTracking[0] = movement.new TitleTracking();
		movement.titleTracking[0].buyer = "Statoil";
		movement.titleTracking[0].seller = "Shell";
		movement.titleTracking[0].titleTransferDate = "18102015T12:12";
		return movement;
	}	
}
