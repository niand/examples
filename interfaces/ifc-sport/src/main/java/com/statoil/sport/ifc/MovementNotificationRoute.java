package com.statoil.sport.ifc;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

@Component
public class MovementNotificationRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("direct:MovementNotificationStart")
			.marshal().json(JsonLibrary.Jackson)
			.multicast().to("file://messages", "activemq:topic:VirtualTopic.MovementUpdated");		
	}

}
