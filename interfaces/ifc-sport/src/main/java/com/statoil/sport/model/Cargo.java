package com.statoil.sport.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Cargo {
	
	@Id
	private long id;
	private String cargoNo;
	private String delNo;
	private String deliveryTerms;
	
	public Cargo() {}
		
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCargoNo() {
		return cargoNo;
	}
	public void setCargoNo(String cargoNo) {
		this.cargoNo = cargoNo;
	}
	public String getDelNo() {
		return delNo;
	}
	public void setDelNo(String delNo) {
		this.delNo = delNo;
	}
	public String getDeliveryTerms() {
		return deliveryTerms;
	}
	public void setDeliveryTerms(String deliveryTerms) {
		this.deliveryTerms = deliveryTerms;
	}

}
