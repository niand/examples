package com.statoil.im.log;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.statoil.im.message.Message;

@Entity
@Table(name = "log")
public class LogEntry {

	@Id	
	public Long id;
	
	@Embedded
	public Message message;		
	public String description;
		
	public LogEntry() {}
	
	public LogEntry(Message message) {		
		this.message = message;
		this.id = System.currentTimeMillis();
	}	
	
	public LogEntry(Message message, String description) {
		this.message = message;
		this.description = description;
		this.id = System.currentTimeMillis();
	}	
	
}
