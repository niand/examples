package com.statoil.im.ifc.queue.model;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * This is just an ApiModel for illustrating how to document a Point-to-Point interface over a queue.
 * 
 * @see QueueReceipient
 *
 */
@ApiModel(description="A person sent over a queue")
public class Person {
	
	// Example structure
	
	@ApiModelProperty(value="First name of the person", example="Nils", required=true)
	public String firstName;
	@ApiModelProperty(value="Last name of the person", example="Andersen", required=true)
	public String lastName;
	@ApiModelProperty(value="Address structure")
	public Address address;
	@ApiModelProperty(value="Birthday", example="01.01.2000")
	public Date birthday;
	@ApiModelProperty(value="Number of children", example="2")
	public int numberOfChildren;
	
	class Address {
		@ApiModelProperty(value="Name of the street, w/o street number", example="StreetA")
		public String street;
		@ApiModelProperty(value="Streetnumber, including letters if applicable", example="11B")
		public String streetNumber;
		@ApiModelProperty(value="Name of the city", example="Oslo")
		public String city;
		@ApiModelProperty(value="Name of the country", example="Norway")
		public String country;
	}
	

}
