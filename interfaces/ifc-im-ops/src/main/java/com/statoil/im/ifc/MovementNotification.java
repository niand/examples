package com.statoil.im.ifc;

/**
 * This class is used for deserializing a MovementNotification coming from SPORT. 
 * The structure of the notification is owned by SPORT. 
 *
 */
public class MovementNotification {
	
	public String cargoNo;
	public String deliveryNo;
	public String deliveryTerms;

}
