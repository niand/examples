package com.statoil.im.ifc.queue;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.statoil.im.ifc.queue.model.Person;

import io.swagger.annotations.ApiOperation;

/**
 * This is just a dummy class that will never be invoked in this examples app. 
 * The purpose is just to show how to document a point to point interface over a queue. 
 * By definition, a point to point interface over a queue is owned by the 
 * Recipient and thus documented here. The analogy to a REST interface is a HTTP POST. 
 *
 * @see Person
 */
@Controller
public class QueueReceipient  {

	@ApiOperation(value="Process a Person message")
	@PostMapping("jms:personqueue")
	public void handlePerson(@RequestBody Person message) {
		
		// do nothing
		
	}
}
