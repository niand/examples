package com.statoil.im.service;


import java.util.Map;

import org.apache.camel.Consume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.statoil.im.ifc.Movement;
import com.statoil.im.ifc.MovementNotification;
import com.statoil.im.ifc.MovementRepository;
import com.statoil.im.log.LogEntry;
import com.statoil.im.log.LogRepository;
import com.statoil.im.message.Message;

@Service
public class IMService {
		
	@Autowired
	private LogRepository repo;
	
	@Autowired
	private MovementRepository movementRepo;
	
	@Autowired
	private RestTemplate sportClient;
		
	@Consume(uri = "direct:MovementNotificationReceived")
	public void movementNotificationReceived(Message notificationMessage) {
		logMessage(notificationMessage, "Received notification");
		processNotification(notificationMessage);
	}
	
	private Movement getMovementFromSport(Message notificationMessage) {
		// Build a WS call, note that we include correlation- and causation id
		// in the header
		
		Message requestMessage = Message.createFrom(notificationMessage);	
		logMessage(requestMessage, "Synchronous Movement Request");
		MovementNotification notification = notificationMessage.getPayload(MovementNotification.class);		
				
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("CorrelationID", requestMessage.correlationId);
		headers.set("MessageID", requestMessage.messageId);
		headers.set("CausationID", requestMessage.causationId);

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl("http://localhost:8082/sport/api/movement")
				.queryParam("cargoNo", notification.cargoNo)
				.queryParam("delNo", notification.deliveryNo);

		HttpEntity<?> entity = new HttpEntity<>(headers);

		ResponseEntity<Movement> response =	sportClient.exchange(
						builder.build().encode().toUri(), 
						HttpMethod.GET,
						entity, 
						Movement.class);
		
		Message reponseMessage = Message.recreate(
				response.getHeaders().getFirst("MessageID"),
				response.getHeaders().getFirst("CorrelationID"),
				response.getHeaders().getFirst("CausationID"));
				
		logMessage(reponseMessage, "Synchronous Movement Response");

		return response.getBody();
	}
	
	@Transactional
	private void processNotification(Message notificationMessage) {
		// Check some criteria
		
		Movement movement = getMovementFromSport(notificationMessage);		
		movementRepo.save(movement);
		
	}
	

	@Transactional
	private void logMessage(Message message, String description) {
		
		
		LogEntry logEntry = 
				new LogEntry(message, description);
			
		repo.save(logEntry);								
	}

	public Page<LogEntry> getNotifications(int page, int pageSize) {
		return repo.findAll(new PageRequest(page, pageSize));
	}

	public Page<Movement> getMovements(int page, int pageSize) {
		return movementRepo.findAll(new PageRequest(page, pageSize));
	}
		
}
