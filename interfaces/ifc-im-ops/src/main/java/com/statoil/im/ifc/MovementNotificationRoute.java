package com.statoil.im.ifc;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import com.statoil.im.message.Message;

/**
 * A Camel route picking up MovementUpdated notification and forwards them to
 * a handler method annotated with "direct:MovementNotificationReceived"
 * 
 * @see IMService
 * @see MovementUpdated
 *
 */
@Component
public class MovementNotificationRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("activemq:queue:Consumer.IM.VirtualTopic.MovementUpdated")
			.unmarshal().json(JsonLibrary.Jackson, Message.class)    
			.to("direct:MovementNotificationReceived");		
	}

}

