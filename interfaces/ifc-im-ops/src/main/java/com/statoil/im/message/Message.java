package com.statoil.im.message;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class is just a utility class. The purpose of this class is to help copy relevant Id's from
 * a request to a response, plus providing a fluent API for message creation and serializing/deserializing
 * between JSON and Java Objects.  
 * 
 */
@Embeddable
final public class Message {	
	public String causationId;
	public String messageId;
	public String correlationId;
	
	@Column(length=4000)
	public String payload;	
	
	public class PayloadGenerator {
		private HashMap<String, String> map;
		private ObjectMapper mapper;
	
		public PayloadGenerator add(String key, String value) {
			if (map == null) {
				map = new HashMap<String, String>();
			}
			map.put(key, value);
			return this;
		}
		
		public Message done() {
			mapper = new ObjectMapper();
			try {
				Message.this.payload = mapper.writeValueAsString(map);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			return Message.this;
		}
	}
	
	public static Message createNew() {
		Message message = new Message();
		message.messageId = UUID.randomUUID().toString();
		message.correlationId = UUID.randomUUID().toString();
		return message;
	}
	
	public static Message createFrom(Message previous) {
		Message message = new Message();
		message.messageId = UUID.randomUUID().toString();
		message.causationId = previous.messageId;
		message.correlationId = previous.correlationId;
		return message;
	}
	
	public static Message recreate(String messageId, String correlationId, String causationId) {
		Message message = new Message();
		message.messageId = messageId;
		message.correlationId = correlationId;
		message.causationId = causationId;
		return message;
	}
	
	public PayloadGenerator payload() {
		return new PayloadGenerator();
	}
		
	
	public Message payload(String json) {		
		this.payload = json;
		return this;
	}
	
	public Message payload(Object obj) {		
		ObjectMapper mapper = new ObjectMapper();
		try {
			payload = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return this;
	}
	
	
	public <T> T getPayload(Class<T> type) {
		ObjectMapper mapper = new ObjectMapper();
		T payloadObject = null;
		try {
			payloadObject = (T) mapper.readValue(payload, type);
		} catch (IOException e) {			
			e.printStackTrace();			
		}
		return payloadObject;
	}
	
	public Map<String, Object> getPayloadMap() {
		if (payload == null) {
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> retVal = null;
		try {
			retVal = mapper.readValue(payload, new TypeReference<Map<String,Object>>() {});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return retVal;
	}
}
