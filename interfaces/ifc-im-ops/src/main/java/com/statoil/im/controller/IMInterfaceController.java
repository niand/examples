package com.statoil.im.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.statoil.im.service.IMService;

/**
 * Web Controller hosting the main page
 *
 */
@Controller
public class IMInterfaceController {

	@Autowired
	private IMService statusService;

	
	@GetMapping("/")
	public String status(Model model) {
		model.addAttribute("notifications", statusService.getNotifications(0,20));
		model.addAttribute("movements", statusService.getMovements(0,20));
		return "status";
	}	

}
