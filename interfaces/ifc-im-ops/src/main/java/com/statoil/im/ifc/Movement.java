package com.statoil.im.ifc;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * This class is used for deserializing JSON coming from SPORT representing a Movement.
 * It is also being persisted, so in this case there is no Anti-Corruption layer.  
 * 
 * Note that "IM" is a client of this interface, so the master definition of a Movement is found on the SPORT side. 
 * 
 */
@Entity
public class Movement {
			
	public class Quantity {		
		public Double qty;
		public String unit;			
		public String type;
	}
	
	@Embeddable
	public class PhysicalMovement {		
		public String quality;
		public Quantity[] quantities;
	}
	
	public class FieldSplit {
		String field;
		public Quantity[] quantities;
		public Double blPercentage;			
	}
		
	public class Origin {			
		public String origin;
		public Quantity[] quantities;
		public Double blPercentage;
		public FieldSplit[] fieldSplit;
	}
	
	public static class TitleTracking {
			public TitleTracking() {}
			public String seller;
			public String buyer;
			public String titleTransferDate;
			public int sequence;
	}			
	
	public String message;
	public String messageVersion;
	public String referenceKey;	
	
	@Id
	public String movementReferenceKey;	
	public String sourceSystem;	
	public String state;
	public String storage;	
	public String transportMode;	
	public String vessel;
	public String codDate;
	public String norDate;
	public String blDate;
		
	@Embedded
	public PhysicalMovement physicalMovement;
	@Transient
	public Origin[] origin;
	@Transient
	public TitleTracking[] titleTracking;
	
}
