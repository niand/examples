package com.statoil.im.log;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

public interface LogRepository extends Repository<LogEntry, UUID> {
	
	Page<LogEntry> findAll(Pageable pageable);
	LogEntry findOne(UUID id);
	LogEntry save(LogEntry entry);	
}
