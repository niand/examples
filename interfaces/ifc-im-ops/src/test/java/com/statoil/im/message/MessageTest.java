package com.statoil.im.message;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.statoil.im.ifc.MovementNotification;

public class MessageTest {
	
	@Test
	public void verifyPayload() {
		MovementNotification notif = new MovementNotification();
		notif.cargoNo = "1";
		notif.deliveryNo = "2";
		notif.deliveryTerms = "CIF";
		
		Message message = Message.createNew().payload(notif);
		
		MovementNotification notif2 = message.getPayload(MovementNotification.class);
		
		assertTrue("cargoNo", notif.cargoNo.equals(notif2.cargoNo));
		assertTrue("deliveryNo", notif.deliveryNo.equals(notif2.deliveryNo));
		assertTrue("deliveryTerms", notif.deliveryTerms.equals(notif2.deliveryTerms));			
	}

}
