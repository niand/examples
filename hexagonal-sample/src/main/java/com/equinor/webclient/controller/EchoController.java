package com.equinor.webclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.equinor.domain.EchoService;

@Controller
public class EchoController {

	@Autowired
	private EchoService echoService;
	
	@GetMapping("/")
	public String cargo(Model model) {
		model.addAttribute("list", echoService.listAll());
		return "index";
	}
	
}
