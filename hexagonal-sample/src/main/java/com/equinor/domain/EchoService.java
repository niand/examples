package com.equinor.domain;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.equinor.persistence.AppDbResolver;

@Service
@Transactional
public class EchoService {
	
	@Autowired
	private AppDbResolver db;
	
	public String echoMessage(String text) {
		EchoMessage message = EchoMessage.create(text);
		db.getCurrentRepo().saveMessage(message);
		return "Echo: " + text;
	}

	public List<EchoMessage> listAll() {
		if (db.getCurrentRepo().list().isPresent()) {
			return db.getCurrentRepo().list().get();
		} 
		return null;
	}	
}
