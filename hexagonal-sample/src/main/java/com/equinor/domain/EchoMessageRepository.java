package com.equinor.domain;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * "Port" for handling persistence
 * 
 * @author NIAND
 *
 */
public interface EchoMessageRepository {
	
	EchoMessage saveMessage(EchoMessage message);
	
	Optional<EchoMessage> findById(UUID id);
	
	Optional<List<EchoMessage>> list();
}
