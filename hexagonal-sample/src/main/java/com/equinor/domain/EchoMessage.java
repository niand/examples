package com.equinor.domain;

import java.time.LocalDateTime;
import java.util.Locale;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

@DomainEntity	
@Entity
public class EchoMessage {

	@Id
	public UUID id;
	public String text;
	public LocalDateTime timestamp;
	
	public static EchoMessage create(String messageText) {
		EchoMessage message = new EchoMessage();
		message.text = messageText;
		message.id = UUID.randomUUID();
		message.timestamp = LocalDateTime.now();
		return message;
	}
	
	public String toString() {
		return String.format(Locale.US, "%-30s | %s - (%s)", text, timestamp.toString(), id);
	}
	
}
