package com.equinor.persistence;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.equinor.domain.EchoMessage;

public interface EchoMessageRepositoryJPA extends JpaRepository<EchoMessage, UUID> {

}
