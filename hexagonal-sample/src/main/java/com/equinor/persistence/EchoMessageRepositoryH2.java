package com.equinor.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.equinor.domain.EchoMessage;
import com.equinor.domain.EchoMessageRepository;

@Repository
public class EchoMessageRepositoryH2 implements EchoMessageRepository  {
	
	@Autowired
	private EchoMessageRepositoryJPA jpaRepo;

	@Override
	public EchoMessage saveMessage(EchoMessage message) {
		return jpaRepo.save(message);
	}

	@Override
	public Optional<EchoMessage> findById(UUID id) {
		return jpaRepo.findById(id);
	}

	@Override
	public Optional<List<EchoMessage>> list() {
		return Optional.of(new ArrayList<EchoMessage>(jpaRepo.findAll()));
	}
}
