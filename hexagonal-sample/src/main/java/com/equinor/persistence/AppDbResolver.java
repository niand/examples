package com.equinor.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.equinor.domain.EchoMessageRepository;

@Component()
public class AppDbResolver {
		
	private EchoMessageRepository h2Backend;
	
	private EchoMessageRepository nitriteBackend;
	
	private EchoMessageRepository currentRepo;
	private String currentDesc;
		
	@Autowired
	public void setEchoMessageRepositoryNitrite(EchoMessageRepositoryNitrite nitrite) {
		nitriteBackend = nitrite;
		setNitrite();
	}
	
	
	@Autowired
	public void setEchoMessageRepositoryH2(EchoMessageRepositoryH2 h2) {
		h2Backend = h2;		
	}
	
	public void setNitrite() {
		currentRepo = nitriteBackend;
		currentDesc = "Nitrite Document DB backend";
	}
	
	public void setH2() {
		currentRepo = h2Backend;
		currentDesc = "H2 Relational DB backend";
	}

	public EchoMessageRepository getCurrentRepo() {
		return currentRepo;
	}
	
	public String getCurrentDbDesc() {
		return currentDesc;
	}
	
}
