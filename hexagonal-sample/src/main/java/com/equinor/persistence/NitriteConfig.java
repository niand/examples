package com.equinor.persistence;

import org.dizitart.no2.Nitrite;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NitriteConfig {
	
	@Bean
	public Nitrite nitriteInMemoryDb() {
		return Nitrite.builder().openOrCreate();
	}
	
	@Bean
	public EchoMessageNitriteMapper echoMessageNitriteMapper() {
		return new EchoMessageNitriteMapper();
	}

}
