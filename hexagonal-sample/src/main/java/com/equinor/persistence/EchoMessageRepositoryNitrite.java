package com.equinor.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.dizitart.no2.Cursor;
import org.dizitart.no2.Document;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.NitriteCollection;
import org.dizitart.no2.filters.Filters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.equinor.domain.EchoMessage;
import com.equinor.domain.EchoMessageRepository;

@Repository
public class EchoMessageRepositoryNitrite implements EchoMessageRepository {

	@Autowired
	private Nitrite db;
	
	@Autowired
	private EchoMessageNitriteMapper mapper;
	
	@Override
	public EchoMessage saveMessage(EchoMessage message) {
		NitriteCollection collection = db.getCollection("echoMessages");
		collection.insert(mapper.toDocument(message));
		return message;
	}

	@Override
	public Optional<EchoMessage> findById(UUID id) {
		NitriteCollection collection = db.getCollection("echoMessages");
		Cursor cursor = collection.find(Filters.eq("message_id", id.toString()));
		return Optional.ofNullable(mapper.toMessage(cursor.firstOrDefault()));		
	}

	@Override
	public Optional<List<EchoMessage>> list() {
		NitriteCollection collection = db.getCollection("echoMessages");
		Cursor cursor = collection.find();		
		List<EchoMessage> result = cursor.totalCount() > 0 ? new ArrayList<EchoMessage>() : null;
		for (Document document : cursor) {
			result.add(mapper.toMessage(document));
		}
		return Optional.ofNullable(result);
	}
}
