package com.equinor.persistence;

import java.time.LocalDateTime;
import java.util.UUID;

import org.dizitart.no2.Document;

import com.equinor.domain.EchoMessage;

public class EchoMessageNitriteMapper {
	
	Document toDocument(EchoMessage message) {
		return Document.createDocument("message_id", message.id.toString())
				.put("text", message.text)
				.put("timestamp", message.timestamp.toString());
	}
	
	EchoMessage toMessage(Document doc) {
		if (doc == null) {
			return null;
		}
		EchoMessage message = new EchoMessage();
		message.id = UUID.fromString(doc.get("message_id", String.class));		
		message.text = doc.get("text", String.class);
		message.timestamp = LocalDateTime.parse(doc.get("timestamp", String.class));
		return message;
	}

}
