package com.equinor.shellclient;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import com.equinor.domain.EchoMessage;
import com.equinor.domain.EchoService;
import com.equinor.persistence.AppDbResolver;

@ShellComponent
public class EchoShell {

	@Autowired
	private EchoService echoService;
	
	@Autowired
	private AppDbResolver db;
	
	@ShellMethod("Echo an input string")
	public String echo(String text) {
		return echoService.echoMessage(text);
	}
	
	@ShellMethod("List the messages")
	public List<EchoMessage> list() {
		return echoService.listAll();
	}
	
	@ShellMethod("Config")
	public List<String> config(@ShellOption(defaultValue="list") String detail) {
		if ("list".equals(detail)) {
			return Arrays.asList(
					"Usage:", 
					"config nitrite - Set nitrite DB", 
					"config h2 - Set H2 DB",
					"-----------------------------------",
					"Current database: " + db.getCurrentDbDesc());
		} else if ("nitrite".equals(detail)) {
			db.setNitrite();
			return Arrays.asList("Nitrite DB");
		} else if ("h2".equals((detail))) {
			db.setH2();
			return Arrays.asList("H2 DB");
		}
		
		return Arrays.asList("Unknown command");
	}
	
	
}
